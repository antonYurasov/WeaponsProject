﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelicopterController : MonoBehaviour {


    public float MainBladesSpeed = 100;
    public float TailBladesSpeed = 100;

    public Transform MainBladesObject;
    public Transform FirstTailBladesObject;
    public Transform SecondTailBladeObject;

    public List<ParticleSystem> FireParticles = new List<ParticleSystem>();

    public bool onAwake;

	// Use this for initialization
	void Start () {
       if(onAwake)
        {
            StartRotating();
        }
	}

    public void StartRotating()
    {
        StartCoroutine(Rotate());
    }

    private IEnumerator Rotate()
    {
        while(true)
        {
            if(MainBladesObject)
            {
                MainBladesObject.rotation *= Quaternion.AngleAxis(10*MainBladesSpeed * Time.deltaTime, Vector3.forward);
            }
            if(FirstTailBladesObject)
            {
                FirstTailBladesObject.rotation *= Quaternion.AngleAxis(10*TailBladesSpeed * Time.deltaTime, Vector3.right);
            }
            if(SecondTailBladeObject)
            {
                SecondTailBladeObject.rotation *= Quaternion.AngleAxis(10*-TailBladesSpeed * Time.deltaTime, Vector3.right);
            }

            yield return null;
        }
    }

    private void TurnOnParticles(int index)
    {
        FireParticles[index].Play();
    }

	
	
}
