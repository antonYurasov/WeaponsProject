﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubController : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine(Rotate());
	}

    public float speed = 40;

    private IEnumerator Rotate()
    {
        while(true)
        {
            this.transform.rotation *= Quaternion.AngleAxis(Time.deltaTime * speed, Vector3.forward);
            yield return null;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
