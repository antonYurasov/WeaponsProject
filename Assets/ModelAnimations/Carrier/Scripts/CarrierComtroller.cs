﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarrierComtroller : MonoBehaviour {

    public float BladesSpeed = 50;
    public float CarrierSpeed = 50;
    public Transform Blades;
    public List<ParticleSystem> particles = new List<ParticleSystem>();
    public float delayBetweenShots = 2;

	// Use this for initialization
	void Start () {
        StartCoroutine(Rotate());
        StartFire();
	}

    public void StartFire()
    {
        StartCoroutine(Fire());
    }

    private IEnumerator Rotate()
    {
        while(true)
        {
            Blades.rotation *= Quaternion.AngleAxis(10*BladesSpeed * Time.deltaTime,Vector3.up);
            this.transform.rotation *= Quaternion.AngleAxis(CarrierSpeed * Time.deltaTime, Vector3.up);
            yield return null;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private IEnumerator Fire()
    {
       
        while(true)
        {
            foreach (var particle in particles)
            {
                particle.Play();
            }
            yield return new WaitForSeconds(delayBetweenShots);
        }

        
    }

   
}
