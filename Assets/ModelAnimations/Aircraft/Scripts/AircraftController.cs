﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AircraftController : MonoBehaviour {


    public List<ParticleSystem> particles = new List<ParticleSystem>();

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void Fire(int index)
    {
        particles[index].Play();
    }

    private void Stop(int index)
    {
        particles[index].Stop();
    }
}
